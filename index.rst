.. eT documentation documentation master file, created by
   sphinx-quickstart on Fri Dec  6 11:21:39 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

eT
====

.. toctree::
   :maxdepth: 1
   :hidden:
   
   user_manual.rst
   features.rst
   about_eT.rst
   research.rst
   workshop.rst

