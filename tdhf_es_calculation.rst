Time-dependent Hartree-Fock excited states calculation
======================================================
The computation of Hartree-Fock excitation energies and oscillator strengths are currently available for restricted HF and QED-HF. A description of how to perform generic HF and QED-HF calculations are given in the ``Hartree-Fock calculation`` and ``QED-HF calculation`` tutorial. In addition to the sections that are needed for a ground state HF or QED-HF calculation, the ``tdhf excited state`` keyword must be specified in the ``do`` section:

.. code-block:: none

  do
     tdhf excited state
  end do

Furthermore, you must add the :ref:`solver tdhf es section <solver-tdhf-es-section>` and specify the number of singlet states, for instance:

.. code-block:: none

  solver tdhf es
     singlet states: 2
  end solver tdhf es

In this same section you can specify, among other keywords, the ``restart``, ``max iterations``, ``tamm-dancoff`` and threshold keywords.
A minimal working example is:

.. code-block:: none

  system
     name: methanol
     charge: 0
  end system
  
  do
     tdhf excited state
  end do
  
  memory
     available: 8
  end memory
  
  method
     hf
  end method
  
  solver tdhf es
    singlet states: 2
  end solver tdhf es
  
  geometry
  basis: cc-pVDZ
  H        0.8475370000      0.0347360000      1.0345270000
  C        0.3504820000      0.0067450000      0.0608350000
  H        0.6350750000      0.8927640000     -0.5200650000
  H        0.6629360000     -0.8933800000     -0.4828330000
  O       -1.0108350000     -0.0082340000      0.3643870000
  H       -1.4851950000     -0.0326310000     -0.4568520000
  end geometry


If successful, you should find a summary of the excitation energies, transition dipole moments and oscillator strengths at the end of the output file:

.. code-block:: none 

  - TDHF excitation energies:

                                      Excitation energy
                          ------------------------------------------
      State                (Hartree)             (eV)
     ---------------------------------------------------------------
        1                  0.330048904040        8.981088133311
        2                  0.406144230368       11.051747432337
     ---------------------------------------------------------------
     eV/Hartree (CODATA 2014): 27.21138602

  - Summary of TDHF transition properties calculation:

     States m = 0 and n = 1:
     -------------------------
     Excitation energy [E_h]:           0.330048904040
     Excitation energy [eV]:            8.981088133311

                Transition moments [a.u.]   Transition strength [a.u.]
     ------------------------------------------------------------------
     Comp. q            <m|q|n>                   |<m|q|n>|^2
     ------------------------------------------------------------------
       X             0.0003509311                0.0000001232
       Y            -0.0227066964                0.0005155941
       Z             0.0004825970                0.0000002329
     ------------------------------------------------------------------
     Oscillator strength:      0.000113525847

     States m = 0 and n = 2:
     -------------------------
     Excitation energy [E_h]:           0.406144230368
     Excitation energy [eV]:           11.051747432337

                Transition moments [a.u.]   Transition strength [a.u.]
     ------------------------------------------------------------------
     Comp. q            <m|q|n>                   |<m|q|n>|^2
     ------------------------------------------------------------------
       X             0.0838600158                0.0070325022
       Y            -0.0015004221                0.0000022513
       Z            -0.1261278272                0.0159082288
     ------------------------------------------------------------------
     Oscillator strength:      0.006212106595

For QED-HF we also provide an esitmation of the photon character of the excitation.


Relevant input sections
-----------------------
:ref:`solver tdhf es <solver-tdhf-es-section>`
