
solver scf geoopt
-----------------

Keywords related to the HF geometry optimization solver go in here.

Keywords
^^^^^^^^

.. container:: sphinx-custom

   ``algorithm: [string]``
   
   Default: ``bfgs``
   
   Selects the solver to use. Optional. 
   
   Valid keyword values are:
   
   - ``bfgs`` A Broyden-Fletcher-Goldberg-Shanno (BFGS) solver using redundant internal coordinates and a rational function (RF) level shift obtained from an augmented Hessian.

.. container:: sphinx-custom

   ``max step: [real]``
   
   Default:
   :math:`0.5` (or *0.5d0*)
   
   Maximum accepted step length in :math:`L^{2}`-norm. Rescales the step to the boundary otherwise. Optional.

.. container:: sphinx-custom

   ``energy threshold: [real]``

   Default: :math:`10^{-4}` (or 1.0d-4)

   Energy convergence threshold, as measured with respect to the previous iteration. Optional. 

.. container:: sphinx-custom

   ``gradient threshold: [real]``

   Default: :math:`10^{-4}` (or 1.0d-4)

   Threshold of the :math:`L^{2}`-norm of the energy gradient. Optional.

.. container:: sphinx-custom

   ``max iterations: [integer]``

   Default: 100

   The maximum number of iterations. The solver stops if the number of iterations exceeds this number. 
   Optional.

.. container:: sphinx-custom

   ``restart``

   Default: false

   If specified, the solver will attempt to restart from a previous calculation. Optional.

.. container:: sphinx-custom

   ``forward difference``

   utilizes numerical gradients of type forward difference instead of analytical gradient

   .. container:: sphinx-custom

   ``central difference``

   utilizes numerical gradients of type central difference instead of analytical gradient
