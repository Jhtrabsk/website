qed
---

General QED keywords.

Required keywords
^^^^^^^^^^^^^^^^^

.. container:: sphinx-custom

   ``coupling: {[real], ...}``

      Default: `0.0`

   Specifies the light-matter coupling :math:`\lambda= \sqrt{4\pi/V_{\text{eff}}}` in atomic units.

.. container:: sphinx-custom

   ``frequency: {[real], ...}``

   Specifies the photon frequency/energy (:math:`\omega`) in atomic units.

.. container:: sphinx-custom

   ``modes: [integer]``

   Specifies the number of photon modes in the QED calculation.

.. container:: sphinx-custom

   ``polarization: {[real],[real],[real]}``

   Specifies the transversal polarization vector (:math:`\vec{\epsilon}`).

.. container:: sphinx-custom

   ``wavevector: {[real],[real],[real]}``

   Specifies the direction of the wavevector (:math:`\vec{k}`).

   .. note::

      The number of modes must be even.

.. note::

   Either the polarization or wavevector must be specified, but not both.

Optional keywords
^^^^^^^^^^^^^^^^^

.. container:: sphinx-custom

   ``coupling bilinear: {[real], ...}``

   Overwrites the bilinear light-matter coupling :math:`\lambda \sqrt{\omega/2}`.

.. container:: sphinx-custom

   ``coupling self: {[real], ...}``

   Overwrites the light-matter self-coupling :math:`0.5 \lambda^2`.

.. container:: sphinx-custom

   ``coherent state: {[real], ...}``

   Specifies the coherent state for each photon mode.

.. container:: sphinx-custom

   ``hf coherent state: {[real], ...}``

   Specifies the coherent state for each photon mode. Only used in QEDHF.

.. container:: sphinx-custom

   ``quadrupole oei``

   Assume complete basis (:math:`\sum_p |p\rangle\langle p| = 1`) to rewrite the self-interaction
   with quadrupole moments, removing references to the virtual density.

   .. note::

      Decreases accuracy in a finite basis. Without this option `frozen core` is disabled.
