active atoms
------------

To specify active atom regions the ``active atoms`` section is required.

Keywords
^^^^^^^^

.. container:: sphinx-custom

	``selection type: [string]``
   
	Keyword to give the type of atom selection. Required keyword.

	Valid keyword values are:

   - ``list`` A list of atoms will be given
   - ``range`` A range of atoms will be given
   - ``central atom`` A central atom and a radius will be given

.. container:: sphinx-custom

   ``[method string]: [list, range or radius]``

   Keyword to give the list, range or radius for an active space treated at the given level of theory. 

   .. note::
      This keyword depends on the ``selection type``.

   Valid method strings:

   - ``hf``
   - ``ccs``
   - ``cc2``
   - ``ccsd``
   - ``cc3``
   - ``ccsd(t)``

   A list is specified using set notation: ``{1,2,3}``.

   A range is specified as ``[1,3]`` (equivalent to ``{1,2,3}``).

   A radius is specified by a double precision real (e.g. ``1.0d2``) and is always in Angstrom units. The atoms within the radius :math:`r` of the ``central atom`` then define the active atoms.

   Example:
   First three atoms in the input are chosen as HF active atoms space.

   .. code-block:: none

   		active atoms
   		   selection type: list
   		   hf: {1,2,3}
   		end active atoms


.. container:: sphinx-custom
    
   ``central atom: [integer]``

   Specifies the central atom in the active space. Requested for ``selection type: central atom``.

   .. note::
      This keyword is necessary in case of ``selection type: central atom``.

.. container:: sphinx-custom
    
   ``inactive basis: [string]``

   Specifies the basis set on the inactive atoms, that is the atoms not specified in the ``active atoms`` section.
   Optional.

   Valid keyword values are basis sets available in :math:`e^T`.

.. container:: sphinx-custom
	
	``[method string] basis: [string]``

	Set basis for active space treated at a given level of theory. Optional.

	Valid method strings:

   	- ``hf``
   	- ``ccs``
   	- ``cc2``
   	- ``ccsd``
   	- ``cc3``
   	- ``ccsd(t)``

   	Valid keyword values are basis sets available in :math:`e^T`.

   	Example: First three atoms in the input are chosen as CCSD active atoms space with aug-cc-pVDZ basis.
      
   	.. code-block:: none

   		active atoms
   		   selection type: range
   		   ccsd: [1,3]
   		   ccsd basis: aug-cc-pVDZ
   		end active atoms





