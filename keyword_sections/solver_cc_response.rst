solver cc response
------------------

Keywords related to solving the coupled cluster response equations. Used when solving the amplitude response and multiplier response equations. 

Keywords
^^^^^^^^

.. container:: sphinx-custom

   ``threshold: [real]``

   Default: :math:`10^{-3}` (or 1.0d-3)

   Residual norm threshold for convergence. Optional.

.. container:: sphinx-custom

   ``storage: [string]``

   Default: ``disk``

   Storage for Davidson records. Optional.

   Valid options are:

   - ``disk`` Store records on file.
   - ``memory`` Store records in memory.

.. container:: sphinx-custom

   ``max iterations: [real]``

   Default: :math:`100`

   The maximum number of iterations. The solver stops if the number of iterations exceeds this number. 
   Optional.

