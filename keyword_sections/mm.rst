molecular mechanics
-------------------

MM specific keywords enter into the ``molecular mechanics`` section.

Keywords
^^^^^^^^

.. container:: sphinx-custom

   ``forcefield: [string]``
   
   Default: ``non-polarizable``
   
   Specifies the forcefield to be used for the MM portion.
   
   Valid keyword values are:
   
   - ``non-polarizable`` Electrostatic QM/MM Embedding
   
      .. note::
         The charge has to be provided for each atom in the :ref:`geometry section<geometry-section>`.
   
   - ``fq`` Fluctuating Charge force field.
   
      .. note::
         The electronegativity and chemical hardness has to be provided for each atom in the :ref:`geometry section<geometry-section>`.

.. container:: sphinx-custom

   ``algorithm: [string]``
   
   Default: ``mat_inversion`` 
   
   Selects the algorithm to be used to solve the FQ equation. So far only the inversion algorithm is implemented. Optional.

