.. _anatomy:

Anatomy of an input file
========================

Below is an example of an input file
from :ref:`the HF tutorial<hf-calc>` with some extra comments. 


.. code-block:: none

  system
     name: H2O
     charge: 0
  end system

  !A comment
  do
     ground state
  end do

  !Another comment
  method
     hf
     !A third comment
  end method

  geometry
  basis: cc-pVDZ
  H          0.86681        0.60144        0.00000
  H         -0.86681        0.60144        0.00000
  O          0.00000       -0.07579        0.00000
  end geometry
  
The input consists of a set of sections.
A section begins with the name of the section 
and ends with an end statement.

.. code-block:: none
  
  section name
    ...
  end section name

In the example input above, the ``system``, ``do``, ``method``, and ``geometry`` sections are given. :ref:`These four sections are needed in every input.<required-sections>`

The program will do a sanity check of the input, 
and throw an error if it detects a nonexisting section.
See :ref:`here <list-of-all-keywords>` 
for a list of all sections and keywords.

Keywords are placed in the sections:

.. code-block:: none
  
  section name
    keyword 1: value
    keyword 2
  end section name

Many keywords
have default values and are not required unless you don't want the default.
A few keywords are always required.
In general, 
the set of required keywords and sections will depend on the type of calculation. 

Some keywords require a value.
These keywords are followed by ``:`` and then the value:

.. code-block:: none

  keyword 1: value


Other keywords
do not require a value, e.g. ``ground state`` in the example above.

The input file is not case sensitive.
Whitespaces can be added, for instance to add indentation to your input file. Lines can be commented out using ``!`` (except in the ``geometry`` section). Blank lines between sections are ignored.

The :ref:`geometry section <geometry-section>` must be the final section in the input.


