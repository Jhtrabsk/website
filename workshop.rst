.. _workshop:

eT workshops
============

The next eT workshop will be organized from 12-16 December 2022, at NTNU in Trondheim.
The workshop will be focused on preparation for the second major release of eT.

Attending the workshop is free of charge.

Organizers
^^^^^^^^^^
H\. Koch, A\. C\. Paul, E\. F\. Kjønstad, S\. D\. Folkestad


For details
^^^^^^^^^^^
Send email to support@etprogram.org
